const user = {name:'Juan'};
user.name = 'Luis';
console.log(user.name);

//ejercicio 1

function f(x,y=2,z=7) {
    return x+y+z;
}
console.log(f(5,undefined))

//ejercicio 2

var animal = 'kitty';

var result = (animal === 'kitty') ? 'cute' : 'still nice';

console.log(result);



//ejercicio 3

function kitty(animal) {
    console.log((animal === 'kitty') ? 'cute' : 'still nice');
}

kitty('dog');

//ejercicio 4

function b(a,str = 'not a ') {
    console.log((a === 0) ? (a = 1, str += ' test') : a = 2);
}

b(0);

//ejercicio 5


function a1(a) {
    console.log( a === 1 ? alert('Hey, its is 1!') : 0);
}

a1(1)

//ejercicio 6

function a2(a) {
    console.log( a === 1 ? alert('Hey, it is 1!') : alert('weird,what could it be?'));
    //console.log(if(a === 1) alert('Hey, it is 1!') else alert('weird,what could it be?'))
}
a2(1);


//ejercicio 7

function kitty2(animal) {
    for (var i = 0; i < 5; i++ ){
        //return (animal === 'kitty') ? break : console.log(i);
    }
}

//ejercicio 8

function value(value) {
    switch (value) {
        case 1:
            console.log('I will always run');
            break;
        case 2:
            console.log('I will never run');
            break;
    }
}

//ejercicio 9

function animals(animals) {
    switch (animals) {
        case 'Dog':
            console.log('I will not run since animal !== "Dog"');
            break;
        case 'Cat':
            console.log('I will not run since animal !== "Cat"');
            break;
        default:
            console.log('I will run since animal does not match any other case');
    }
}

//ejercicio 10

function john() {
    return 'John'
}

function jacob() {
    return 'Jacob'
}

switch (name) {
    case john():
        console.log('I will run if name === "John"');
        break;
    case 'Ja'+'ne':
        console.log('I will run if name === "Jane"');
        break;
    case john()+ ' ' + jacob() + ' Jingleheimer Schmidt':
        console.log('His name is equal to name too!');
        break;
}

//ejercicio 11

var x = 'C'
switch (x) {
    case "a":
    case "b":
    case "c":
        console.log("Either a,b or c was selected.");
        break;
    case "d":
        console.log("only d was selected.");
        break;
    default:
        console.log("No case was marched");
        break;


}

//ejercicio 12

function suma() {
    console.log(5+7);
    console.log(5+"7");
    console.log("5"+7);
    console.log(5-7);
    console.log(5-"7");
    console.log("5"-7);
    console.log(5-"x");

}

suma();

//ejercicio 13

var a1 = 'hello' || '';
var b1 = '' || [];
var c1 = '' || undefined;
var d1 = 1 || 5;
var e1 = 0 || {};
var f1 = 0 || '' || 5;
var g1 = '' || 'yay' || 'boo'

//ejercicio 14

var a2 = 'hello' && '';
var b2 = '' && [];
var c2 = undefined && 0;
var d2 = 1 && 5;
var e2 = 0 && {};
var f2 = 'hi' && [] && 'done';
var g2 = 'bye' && undefined && 'adios'


//ejercicio 15

var foo = function (val) {
    return val || 'default';
}

console.log(foo('burger'));
console.log(foo(100));
console.log(foo([]));
console.log(foo(0));
console.log(foo(undefined));


//ejercicio 16

var isLegal = age >=18;
var tall = height => 5.11;
var suitable = isLegal && tall;
var isRoyalty = status == 'royalty';
var specialCase = isRoyalty && hasInvitation;
var canEnterOurBar = suitable || specialCase;

//ejercicio 17

function f3() {
    for (var i = 0; i < 3; i++){
        if (i===1){
            continue;
        }
        console.log(i)
    }
}


//ejercicio 18

function f4() {
    var i = 0;
    while (i<3){
        if (i === 1 ){
            i=2;
            continue;
        }
        console.log(i);
        i++;
    }
}

//ejercicio 19

function f5() {
    for(var i = 0; i < 5; i++){
        nextLoop2Iteration:
            for (var j = 0; j<5; j++){
                if (i == j) break nextLoop2Iteration;
                console.log(i,j)
            }
    }
}

//ejercicio 20


function foo2() {
    var afoo = 'hello';
    function bar() {
        var bfoo = 'world';
        console.log(afoo);
        console.log(bfoo);
    }
    console.log(afoo);
    console.log(bfoo);
}
console.log(afoo);
console.log(bfoo);

//ejercicio 21
function foo3() {
    var a = true;
    function bar() {
        let a = false;
        console.log(a)

    }
    a = false;
    console.log(a);
}

//ejercicio 22
var namedSum = function sum(a,b) {
    return a+b;
}

var anonSum = function (a,b) {
    return a+b;

}

namedSum(1,3);
anonSum(1,3);

//ejercicio 23

var numbers = [1,2,3,8,9,10];
function sliceContat(numbers) {
    return numbers.slice(0,3).concat([4,5,6,7], a.slice(3,6));
}

function splice(numbers) {
   return numbers.splice(3,0, ...[4,5,6,7]);
}

//ejercicio 24

var array = ['a','b','c'];

array.join('->');
console.log(array);
array.join('.');
console.log(array);
'a.b.c'.split('.');

'5.4.3.2.1'.split('.');

//ejercicio 25

var numbersArray = [5,10,15,20,25];

Array.isArray(numbersArray);
numbersArray.includes(10);
numbersArray.includes(10,2);
numbersArray.indexOf(25);
numbersArray.lastIndexOf(10,0);

//ejercicio 26
var letterArray = ['a','b','c','d','e','f'];

letterArray.copyWithin(5,0,1);
letterArray.copyWithin(3,0,3);
letterArray.fill('Z',0,5);

//ejercicio 27

var namesarray = ['Alberto','Ana','Mauricio','Bernardo','Zoe'];

namesarray.sort();
console.log(namesarray);
namesarray = ['Alberto','Ana','Mauricio','Bernardo','Zoe'];

namesarray.reverse();

console.log(namesarray);





